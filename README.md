# Non-perfo-friendly StickMan Animation Engine in JavaSE11 + Swing :

Key A, Z, E ,R are linked to any move that is not supposed to deplace the whole StickMan.   
Key O, K, L, M are linked to move that make your stickman jumps, walks, crounch...  


* Push the `Move Number X` button in order to select the move that you want to record.

* Start a Move Record by clicking on `Record Move X` button before making any move.

* Select/Unselect StickMan joint by clicking on lines that are `StickMan members`.
	* You can select multiples joints by pressing `CTRL Key`  

* Make your move by using `Directionnal panel's buttons`.

* `Reverse CheckBox` makes your move automatically symetrical once recorded.
	* I.E you move 2 time left after recording and adding to model with this checked checkbox, it will move 2 times left then 2 times right  

* After you finished to move all joints you want, add your move to model by pushing the `Add Move X to model` button.

* `Slider` will run your move more or less quickly.  

Preview : ![User Interface](https://gitlab.com/CERI_Raymondaud.Q/2d-stickman-animation-engine/raw/master/screenshot.png "Le seul truc utilisable")
	
