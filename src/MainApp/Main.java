package MainApp;

import javax.swing.SwingUtilities;

import GUI.Window;

public class Main {
	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(()-> { new Window(); });
		
	}
}
