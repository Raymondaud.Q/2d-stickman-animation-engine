package Controller;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Line2D;
import java.util.Vector;

import GUI.CurrentView;
import Model.StickModel;

public class MouseControl implements MouseListener {

	public static final int HIT_BOX_SIZE = 4;
	StickModel model;
	CurrentView win;
	
	public Vector<Integer> selected = new Vector<Integer>(); 
	
	public MouseControl(StickModel m, CurrentView currentView)
	{
		model = m;
		win = currentView;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		int x = e.getX();
	    int y = e.getY();
		int clicked = getClickedLine(x, y);
		int clickedIndex = -1;
		if (clicked != -1 ) {
			if( e.getModifiersEx() == InputEvent.CTRL_DOWN_MASK ) {
			    if (  (clickedIndex = selected.indexOf(clicked)) != -1 )
			    	selected.remove(clickedIndex);
			    else 
			    	selected.add(clicked);
			}
			else{
				selected.clear();
			    selected.add(clicked);
			}
		}
	    win.repaint();
	}
	
	public int getClickedLine(int x, int y) {
		int boxX = x - HIT_BOX_SIZE / 2;
		int boxY = y - HIT_BOX_SIZE / 2;

		int width = HIT_BOX_SIZE;
		int height = HIT_BOX_SIZE;

		for (Line2D line : model.getLines()) {
		    if (line.intersects(boxX, boxY, width, height))
		    {
		    	System.out.println("Line found");
		        return model.getVertex(line); 
		    }
		}

		return -1;
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
