package Controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import GUI.SkillPanel;

public class SkillBoardControl implements KeyListener {
	
		SkillPanel panel;
		public static int dir = -1;
		 public SkillBoardControl (SkillPanel currentSkillPanel) {

			 panel = currentSkillPanel;
		 }


		@Override
		public void keyPressed(KeyEvent evt) {
			
			//panel.interrupt(); // IT MAKES BUG THE CONTROL KEY WHILE SELECTING POINT => REINIT STICKMAN SHAPE ...
			if ( evt.getKeyChar() == 'a' ) {
				dir = 0;
				panel.triggerAction(0);
			}
			else if ( evt.getKeyChar() == 'z' ) {
				dir = 0;
				panel.triggerAction(1);
			}
			else if ( evt.getKeyChar() == 'e' ) {
				dir = 0;
				panel.triggerAction(2);
			}
			else if ( evt.getKeyChar() == 'r' ) {
				dir = 0;
				panel.triggerAction(3);
			}
			
			else if ( evt.getKeyCode() == KeyEvent.VK_M && dir != 3)
			{
				dir = 3;
				panel.triggerAction(4);
			}
			
			else if ( evt.getKeyCode() == KeyEvent.VK_K  && dir != 1)
			{
				dir = 1;
				panel.triggerAction(4);
			}
			
			else if ( evt.getKeyCode() == KeyEvent.VK_L) {
				if ( dir != 4) {
					dir = 4;
					panel.triggerAction(5);
				}
			}
			else if ( evt.getKeyCode() == KeyEvent.VK_O) {
				if ( dir != 2) {
					dir = 2;
					panel.triggerAction(6);
				}
			}
		}

		@Override
		public void keyReleased(KeyEvent evt) {
			
			if  ( evt.getKeyCode() == KeyEvent.VK_M  && dir  == 3) {
				panel.interrupt();
				dir = -1;
			}
			
			else if ( evt.getKeyCode() == KeyEvent.VK_K && dir == 1) {
				panel.interrupt();
				dir = -1;
			}
				
			
			if ( dir != 1 && dir != 3)
				dir = -1;
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	
};
