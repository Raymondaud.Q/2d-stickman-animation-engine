package Model;
import java.io.Serializable;
import java.util.Vector;


public class MoveModel implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private Vector<Move> moveList = new Vector<Move>();
	private int frame = 0;
	
	public void addMove(Move m) {
		moveList.add(m);
	}
	
	public Move get(int index) {
		if ( index < moveList.size() )
			return moveList.get(index);
		return null;
	}
	
	public Move getNextFrame(StickModel model) {
		if ( frame < moveList.size() ) {
			frame+=1;
			return moveList.get(frame-1);
		}
		return null;
	}
	
	public int size() {
		return moveList.size();
	}
	
	public void reinitFrame() { frame = 0; }
	
	//public Vector<Move> getMovement(){ return moveList; }
	

}
