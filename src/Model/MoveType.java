package Model;

import java.io.Serializable;

public enum MoveType implements Serializable{
	UP,
	DOWN,
	RIGHT,
	LEFT
};
