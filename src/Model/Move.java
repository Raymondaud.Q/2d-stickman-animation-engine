package Model;

import java.io.Serializable;

public class Move implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public int pointIndex;
	public MoveType move;  
	
	public Move(int pi, MoveType m) {
		pointIndex = pi;
		move = m;
	}
	
};

