package Model;

import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Vector;

public class StickModel {

	private Vector<Line2D> lines;
	private Vector<Point2D.Double> points;
	private HashMap<Line2D, Integer> vertex;
	private HashMap<String,MoveModel> skills = new HashMap<String,MoveModel>();
	
	private int symmetrical = 1;

	public StickModel() {

		points = new Vector<Point2D.Double>();
		lines = new Vector<Line2D>();
		vertex = new HashMap<Line2D, Integer>();
		
		points.add(new Point.Double(250,278)); // 0 Pied Gauche     
		points.add(new Point.Double(258,257)); // 1 Genoux Gauche   
		points.add(new Point.Double(272,278)); // 2 Pied Droit      
		points.add(new Point.Double(270,257)); // 3 Genoux Droit    
		
		points.add(new Point.Double(260,240)); // 4 Hanche   
		
		points.add(new Point.Double(281,225)); // 5 Coude Gauche    
		points.add(new Point.Double(282,207)); // 6 Main Gauche     
		points.add(new Point.Double(274,229)); // 7 Coude Droit     
		points.add(new Point.Double(274,206)); // 8 Main Droite     
		points.add(new Point.Double(260,210)); // 9 Epaules
		updateLines();
	}

	public Vector<Line2D> getLines() {
		return lines;
	}

	public Vector<Point2D.Double> getPoints() {
		return points;
	}

	public int getVertex(Line2D member) {
		int ret =  vertex.get(member);
		if ( ret == 4 )
			vertex.put(member,9);
		else if ( ret == 9)
			vertex.put(member,4);
		return ret;
	}

	public MoveModel getMove(int index) {
		return skills.get(Integer.toString(index));
	}

	public void setMove(int index, MoveModel m) {
		skills.put(Integer.toString(index), m);
	}
	
	public void allX(double d) {
		for ( Point2D.Double point : points )
			point.setLocation(point.getX() + d, point.getY());
		this.updateLines();
	}

	public Point2D.Double pointX(int index, int x) {
		Point2D.Double cur = points.get(index);
		cur.setLocation(cur.getX() + x, cur.getY());
		return cur;
	}

	public Point2D.Double pointY(int index, int y) {
		Point2D.Double cur = points.get(index);
		cur.setLocation(cur.getX(), cur.getY() + y);
		return cur;
	}

	public void initPoints() {
		
		if ( points.get(4).getY() != 240) 
			points.get(4).setLocation(points.get(4).getX(), 240);
		
		Point2D.Double ref = points.get(4);
		double refX = ref.getX();
		double refY = ref.getY();
		
		points.get(0).setLocation(refX-10*symmetrical, refY+38);
		points.get(1).setLocation(refX-2*symmetrical, refY+17);
		points.get(2).setLocation(refX+12*symmetrical, refY+38);
		points.get(3).setLocation(refX+10*symmetrical, refY+17);
		// point 4 = ref;
		points.get(5).setLocation(refX+21*symmetrical, refY-15);
		points.get(6).setLocation(refX+22*symmetrical, refY-33);
		points.get(7).setLocation(refX+14*symmetrical, refY-11);
		points.get(8).setLocation(refX+14*symmetrical, refY-34);
		points.get(9).setLocation(refX, refY-30);
		
		updateLines();

	}

	public void updateLines() {
		lines.clear();
		
		lines.add(new Line2D.Double(points.get(0), points.get(1))); // Tibia Gauche
		vertex.put(lines.get(0), 0);
		lines.add(new Line2D.Double(points.get(2), points.get(3))); // Tibia Droit
		vertex.put(lines.get(1), 2);
		lines.add(new Line2D.Double(points.get(1), points.get(4))); // Femur Gauche
		vertex.put(lines.get(2), 1);
		lines.add(new Line2D.Double(points.get(3), points.get(4))); // Femur Droit
		vertex.put(lines.get(3), 3);
		lines.add(new Line2D.Double(points.get(4), points.get(9))); // Colonne
		vertex.put(lines.get(4), 4);
		lines.add(new Line2D.Double(points.get(5), points.get(9))); // Humérus Gauche
		vertex.put(lines.get(5), 5);
		lines.add(new Line2D.Double(points.get(7), points.get(9))); // Humérus Droit
		vertex.put(lines.get(6), 7);
		lines.add(new Line2D.Double(points.get(5), points.get(6))); // Avant-Bras Gauche
		vertex.put(lines.get(7), 6);
		lines.add(new Line2D.Double(points.get(7), points.get(8))); // Avant-Bras Droit
		vertex.put(lines.get(8), 8);
	}
	
	public int swapOrientation() {
		symmetrical = symmetrical*-1;
		return symmetrical;
	}
	
	public int getSymmetrical() {
		return symmetrical;
	}
	
	
	// Coefficients de la droite Colonne vertebrale
	public double[] lineCoeff(){
		double [] result = new double[3];
		result[0] = points.get(9).getY() - points.get(4).getY(); // a
		result[1] = points.get(4).getX() - points.get(9).getX(); // b
		result[2] = - result[0] *  points.get(4).getX() - result[1] * points.get(4).getY();
		return result;
	}
	
	// Symetrique des points
	public void symmetrical() {
		double [] abc = lineCoeff();
		for ( int count = 0 ; count < points.size(); count ++ ) {
			points.set(count, new Point.Double(
					(int)( -2 * abc[0] * ( abc[0] * points.get(count).getX() + abc[1] * points.get(count).getY() + abc[2] ) / ( abc[0]*abc[0] + abc[1]*abc[1] ) + points.get(count).getX()),
					(int)( -2 * abc[1] * ( abc[0] * points.get(count).getX() + abc[1] * points.get(count).getY() + abc[2] ) / ( abc[0]*abc[0] + abc[1]*abc[1] ) + points.get(count).getY())
				));
		}
		updateLines();
		
				
	}

}
