package GUI;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

public class ShowViewAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	private String key;
	private CurrentView view;
	
    public ShowViewAction(String name, String key, CurrentView myMain)
    {
        super(name);
        this.key = key;
        this.view = myMain;
    }

	@Override
	public void actionPerformed(ActionEvent arg0) {
		view.changeView(key);
	}

}
