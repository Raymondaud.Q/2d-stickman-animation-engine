package GUI;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.Timer;

import Controller.MouseControl;
import Controller.SkillBoardControl;
import Model.Move;
import Model.MoveModel;
import Model.MoveType;
import Model.Serializer;
import Model.StickModel;

public class SkillPanel extends JPanel {
	
	
	private static final long serialVersionUID = 1L;
	public static final String NAME = "Skill";
	JButton home, up, down, left, right, startMove, addMove, playMove, displayPoints, nextMove;
	JCheckBox reverseCheckBox = new JCheckBox("Reverse move to intial position",true);
	JSlider speed;
	
	JPanel skillsButtons;
	int vertexIndex = -1;
	StickModel model = new StickModel();
	MouseControl mouseControl;
	SkillBoardControl keyboardControl;
	Vector<Timer> actionTimer = new Vector<Timer>();
	
	MoveModel currentMoveModel;
	int moveNumber = 0 ;
	int dir = -1;
	
	Line2D target;
	
	public SkillPanel(CurrentView currentView) {
		super(new FlowLayout(FlowLayout.RIGHT));
		
		target = new Line2D.Double(300,200,300,270);
		
		home = new JButton(new ShowViewAction("Menu",MenuView.NAME,currentView));
		speed = new JSlider(5,25,15);
		displayPoints = new JButton("Display points Coords");
		displayPoints.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				  for (Point2D.Double p : model.getPoints() )
					  System.out.println("points.add(new Point("+p.getX()+","+p.getY()+"));" );
			  } 
		} );
		nextMove = new JButton("Move Number " + moveNumber);
		nextMove.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				  if (currentMoveModel == null)
						try {
							currentMoveModel = (MoveModel) Serializer.load("Move_"+moveNumber+".srzd");
							model.setMove(moveNumber, currentMoveModel);
						} catch (ClassNotFoundException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
				  moveNumber=(moveNumber+1)%7;
				  model.initPoints();
				  for ( Timer action : actionTimer) {
					  action.setInitialDelay(speed.getValue());
					  action.setDelay(speed.getValue());
				  }
				  currentMoveModel = model.getMove(moveNumber);
				  
				  repaint();
			  }
		});
		
		playMove = new JButton("Play Move "+ moveNumber);
		playMove.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				  model.initPoints();
				  currentMoveModel = model.getMove(moveNumber);
				  currentMoveModel.reinitFrame();
				  for ( Timer action : actionTimer) {
					  action.setInitialDelay(speed.getValue());
					  action.setDelay(speed.getValue());
					  action.start();
				  }
			  } 
		} );
		
		startMove = new JButton("Record Move "+ moveNumber);
		startMove.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				  for ( Timer action : actionTimer)
					  action.stop();
				  model.initPoints();
				  currentMoveModel = new MoveModel();
			  } 
		} );
		
		addMove = new JButton("Add Move " + moveNumber + " to model" );
		addMove.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				int size = currentMoveModel.size();
				int index = -1;
				Move m;
				if ( reverseCheckBox.isSelected())
					for(int i = size-1 ; i >= 0; i--) {
						m = currentMoveModel.get(i);
		        		index = m.pointIndex;
		        		if (m.move == MoveType.RIGHT )
		        			currentMoveModel.addMove(new Move(index,MoveType.LEFT));
		        		else if (m.move == MoveType.LEFT )
		        			currentMoveModel.addMove(new Move(index,MoveType.RIGHT));
		        		else  if (m.move == MoveType.DOWN )
		        			currentMoveModel.addMove(new Move(index,MoveType.UP));
		        		else if (m.move == MoveType.UP )
		        			currentMoveModel.addMove(new Move(index,MoveType.DOWN));
		        	}
				try {
					Serializer.save(currentMoveModel,"Move_"+moveNumber+".srzd");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				model.setMove(moveNumber,currentMoveModel);
				model.initPoints();
			}
		} );
		
		
		up = new JButton("🢙");
		up.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				  if ( mouseControl.selected != null )
					  actionUp();
			  } 
		} );
		
		down = new JButton("🢛");
		down.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				  if ( mouseControl.selected != null )
					  actionDown();
			  } 
		} );
		
		
		left = new JButton("🢘");
		left.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				  if ( mouseControl.selected != null )
					  actionLeft();
			  } 
		} );
		
		right = new JButton("🢚");
		right.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				  if ( mouseControl.selected != null )
					  actionRight();
			  } 
		} );
		skillsButtons = new JPanel();
		JPanel south = new JPanel();
		skillsButtons.setLayout(new BorderLayout());
		skillsButtons.add(BorderLayout.NORTH, up);
		skillsButtons.add(BorderLayout.EAST, right);
		skillsButtons.add(BorderLayout.WEST, left);
		
		south.setLayout(new GridLayout(8,0));
		south.add(down);
		south.add(startMove);
		south.add(addMove);
		south.add(reverseCheckBox);
		south.add(playMove);
		south.add(speed);
		south.add(displayPoints);
		south.add(nextMove);
		skillsButtons.add(BorderLayout.SOUTH, south);
		
		mouseControl = new MouseControl(model,currentView);
		keyboardControl = new SkillBoardControl(this);
		
		addMouseListener(mouseControl);
		addKeyListener(keyboardControl);
		
		add(home);
		add(skillsButtons);
		for ( int i = 0 ; i < 10 ; i ++) {
	        actionTimer.add( new Timer(speed.getValue(), (ActionListener) new ActionListener() {
	        	@Override
		        public void actionPerformed(ActionEvent e) {
	        		
	        		Move m = currentMoveModel.getNextFrame(model);
	        		
	        		if ( m == null &&  SkillBoardControl.dir != 3 && SkillBoardControl.dir != 1 )
	        			return;
	        		
	        		else if ( ( SkillBoardControl.dir == 3 || SkillBoardControl.dir == 1 ) &&  m == null ){
	        			currentMoveModel.reinitFrame();
	        			m = currentMoveModel.getNextFrame(model);
	        		}
	        		
	        		int index = m.pointIndex;
	        		if (m.move == MoveType.RIGHT )
	        			model.pointX(index,+1 * model.getSymmetrical());
	        		else if (m.move == MoveType.LEFT )
	        			model.pointX(index,-1 * model.getSymmetrical());
	        		else  if (m.move == MoveType.DOWN )
	        			model.pointY(index,+1);
	        		else if (m.move == MoveType.UP )      			
	        			model.pointY(index,-1 );
	        		
	        		if ( SkillBoardControl.dir == 3 || SkillBoardControl.dir == 1)
	        			model.allX(0.2*model.getSymmetrical());
	        		else
	        			model.updateLines();		        	
	        	}
	        }));
		}
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		nextMove.setText("Move Number " + moveNumber);
		playMove.setText("Play Move "+ moveNumber);
		startMove.setText("Record Move "+ moveNumber);
		addMove.setText("Add Move " + moveNumber + " to model" );
		
		if (model.getMove(moveNumber) != null )
			playMove.setVisible(true);
		else 
			playMove.setVisible(false);
		
		if( currentMoveModel == null )
			addMove.setVisible(false);
		else 
			addMove.setVisible(true);
			
			
        Graphics2D g1 = (Graphics2D) g;
        
        g1.setStroke(new BasicStroke(MouseControl.HIT_BOX_SIZE));
        g1.draw(target);
        
        for ( Line2D line : model.getLines()) {
        	if ( line.intersectsLine(target) ) {
        		g1.setColor(Color.RED);
        		g1.draw(target);
        		g1.setColor(Color.BLACK);
        	}
        	g1.draw(line);
        }
        
        
        int cnt = 0;
        boolean contains = false;
        Vector<Point2D.Double> points = model.getPoints();
        
        g1.setStroke(new BasicStroke((int)(MouseControl.HIT_BOX_SIZE*1.3)));
        for ( Point2D.Double point : points ) {
        	if ( contains = mouseControl.selected.contains(cnt) )
        		g1.setColor(Color.RED);
        	
        	g1.drawLine((int)point.getX(), (int)point.getY(), (int)point.getX(), (int)point.getY());
        	
        	if ( contains ){
        		g1.setColor(Color.BLACK);
        		contains = false;
        	}
        	cnt += 1;
        }
        g1.setStroke(new BasicStroke(MouseControl.HIT_BOX_SIZE));
        
        g.fillOval((int)points.get(9).getX()-7,
        		(int)points.get(9).getY()-15, 
        		15, 15);
        repaint();
        setFocusable(true);
        requestFocusInWindow();
    }
	
	private void actionUp() {
		for ( int point : mouseControl.selected ) {
			System.out.println(point);
			model.pointY(point, -1);
			if ( currentMoveModel != null )
				currentMoveModel.addMove(new Move(point,MoveType.UP));
			model.updateLines();
    	}
	}
	
	private void actionDown() {
    	for ( int point : mouseControl.selected ) {
			model.pointY(point, +1);
			if ( currentMoveModel != null )
				currentMoveModel.addMove(new Move(point,MoveType.DOWN));
			model.updateLines();
    	}	
	}
	
	private void actionLeft() {
		for ( int point : mouseControl.selected ) {
			model.pointX(point,-1);
			if (currentMoveModel != null )
				if ( model.getSymmetrical() == 1)
					currentMoveModel.addMove(new Move(point,MoveType.LEFT));
				else 
					currentMoveModel.addMove(new Move(point,MoveType.RIGHT));
			model.updateLines();
    	}
	}
	
	private void actionRight() {
		for ( int point : mouseControl.selected ) {
			model.pointX(point, +1);
			if ( currentMoveModel != null )
				if ( model.getSymmetrical() == 1)
					currentMoveModel.addMove(new Move(point,MoveType.RIGHT));
				else 
					currentMoveModel.addMove(new Move(point,MoveType.LEFT));
			model.updateLines();
    	}
	}
	
	public void triggerAction(int actionNb )
	{
		moveNumber = actionNb;
		
		if ( SkillBoardControl.dir == 3 && model.getSymmetrical() == -1 || SkillBoardControl.dir == 1 && model.getSymmetrical() == 1 ) {
			model.symmetrical();
			model.swapOrientation();
		}
		
		if (currentMoveModel == null || model.getMove(moveNumber) == null )
			try {
				currentMoveModel = (MoveModel) Serializer.load("Move_"+moveNumber+".srzd");
				model.setMove(moveNumber, currentMoveModel);
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		else if ( currentMoveModel != model.getMove(moveNumber))
			currentMoveModel = model.getMove(moveNumber);
		 
		currentMoveModel.reinitFrame();
		model.initPoints();
	 
		for ( Timer action : actionTimer) {
			action.setInitialDelay(speed.getValue());
			action.setDelay(speed.getValue());
			action.restart();
		}
		  
		repaint();
	}
	
	public void interrupt() {
		for ( Timer action : actionTimer)
			  action.stop();
		model.initPoints();
	}
}
