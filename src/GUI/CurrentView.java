package GUI;

import java.awt.CardLayout;

import javax.swing.JPanel;

public class CurrentView extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private MenuView menu = new MenuView(this);
	
	private CardLayout cardLayout = new CardLayout();
	private GamePanel game = new GamePanel(this);
	private SkillPanel skill = new SkillPanel(this);
	
	public CurrentView(){
		
		this.setLayout(cardLayout);
		add(menu,MenuView.NAME);
		add(game,GamePanel.NAME);
		add(skill,SkillPanel.NAME);
	}
	
	public void changeView(String key)
	{
		if ( key.equals(MenuView.NAME) )
		{
			remove(menu);
			menu = new MenuView(this);
			add(menu,MenuView.NAME);
		}
		
		if ( key.equals(GamePanel.NAME) )
		{
			remove(game);
			game = new GamePanel(this);
			add(game,GamePanel.NAME);
		}
		;
		cardLayout.show(this, key);
	}

}
