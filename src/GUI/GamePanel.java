package GUI;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.Timer;

import Controller.MouseControl;
import Model.StickModel;

public class GamePanel extends JPanel {
	
	
	private static final long serialVersionUID = 1L;
	public static final String NAME = "Game";
	JButton home;
	StickModel model = new StickModel();
	MouseControl mouseControl;
	int angle = 0;
	Timer actionTimer;
	
	public GamePanel(CurrentView currentView) {
		
		home = new JButton(new ShowViewAction("Menu",MenuView.NAME,currentView));
		mouseControl = new MouseControl(model,currentView);
		
		addMouseListener(mouseControl);
		add(home);
		
        actionTimer = new Timer(5, (ActionListener) new ActionListener() {
        	@Override
	        public void actionPerformed(ActionEvent e) {
	        	int elem = 0;
	        	Vector<Point2D.Double> modelPoints = model.getPoints();
	        	for ( Point2D.Double point : modelPoints) {             	
	            	modelPoints.set(elem,new Point2D.Double((int)point.getX()+1,(int)point.getY()+1));
	            	elem+=1;
	        	}
	        	model.updateLines();
	            repaint();
        	}
        });
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        
        g2.setStroke(new BasicStroke(5));
        
        for ( Line2D line : model.getLines())
        	g2.draw(line);
        
        Vector<Point2D.Double> points = model.getPoints();
        for ( Point2D.Double point : points )
        	g2.drawLine((int)point.getX(), (int)point.getY(), (int)point.getX(), (int)point.getY());
        
        g.fillOval((int)points.get(9).getX()-5,
        		(int)points.get(9).getY()-10, 
        		10, 10);
        repaint();
    }


}
