package GUI;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class MenuView extends JPanel{
	

	private static final long serialVersionUID = 1L;
	public static final String NAME = "StickFight Home";
	CurrentView layout;
	
	public MenuView(CurrentView contextLayout) {
		super(new FlowLayout(FlowLayout.CENTER, 3000, 30));
        setName(NAME);
		layout = contextLayout;
		JButton game = new JButton(new ShowViewAction("Game Panel",GamePanel.NAME,layout));
		JButton skill = new JButton(new ShowViewAction("Skill Panel",SkillPanel.NAME,layout));
		game.setEnabled(false);
		add(game);
		add(skill);
		setBorder(BorderFactory.createTitledBorder(null, NAME, TitledBorder.CENTER, TitledBorder.TOP, new Font("times new roman",Font.PLAIN,15), Color.black));
		
	}
	
    @Override
	public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        //g.drawImage(background, 0, 0, this);
        try
        {
            Thread.sleep(50);
        }
        catch (Exception e)
        {

        }

        repaint();
        revalidate();
    }

}
